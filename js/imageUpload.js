//var canvas2 = document.getElementById("canvas2"), //step2 canvas
//    context2 = canvas2.getContext("2d");
//
//var imageLoader = document.getElementById('imageLoader');
//imageLoader.addEventListener('change', handleImage, false);
//
//
//// function responsible for the upload image to canvas
//
//function handleImage(e) {
//
//    var reader = new FileReader();
//    reader.onload = function (event) {
//        var img = new Image();
//        img.onload = function () {
//            var scaleX, scaleY, scale;
//            var scaledWidth, scaledHeight;
//            scaleX = img.width / canvas2.width;
//            scaleY = img.height / canvas2.height;
//            scale = scaleX > scaleY ? scaleX : scaleY;
////            scaledWidth = img.width / scale;
////            scaledWidth = img.width;
//            scaledWidth = img.width;
////            scaledHeight = img.height / scale;
////            scaledHeight = img.height;
//            scaledHeight = img.height;
//            $("#canvas2wrap img").hide();
//			//context2.scale(2, 2);
//            context2.clearRect(0, 0, canvas2.width, canvas2.height);
//            var canvas2image = context2.drawImage(img, (canvas2.width - scaledWidth) / 2, (canvas2.height - scaledHeight) / 2, scaledWidth, scaledHeight);
//            var imgData = context2.getImageData(0, 0, canvas2.width, canvas2.height);
//
//        };
//        img.src = event.target.result;
//    };
//    reader.readAsDataURL(e.target.files[0]);
//}

//function drawImage(imageObj, width, height) {
//    var stage = new Kinetic.Stage({
//        container: "canvas2",
//        width: 900,
//        height: 500
//    });
//    var layer = new Kinetic.Layer();
//
//    // darth vader
//    var darthVaderImg = new Kinetic.Image({
//        image: imageObj,
////         x: stage.getWidth() / 2 - 200 / 2,
////         y: stage.getHeight() / 2 - 137 / 2,
//        width: width,
//        height: height,
//        draggable: true
//    });
//
//    // add cursor styling
//    darthVaderImg.on('mouseover', function () {
//        document.body.style.cursor = 'pointer';
//    });
//    darthVaderImg.on('mouseout', function () {
//        document.body.style.cursor = 'default';
//    });
//
//    layer.add(darthVaderImg);
//    stage.add(layer);
//
//    active_tab = $('#imageBackCont').find('.active').attr('id');
//    if (active_tab == "backSelect") {
//        $('.kineticjs-content > canvas').css('z-index', 300);
//    }
//}

var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);


// function responsible for the upload image to canvas

function handleImage(e) {

    var reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
            $('#canvasImg').attr('src', img.src);
            $('#canvasImg').width(img.width);

        };
        img.src = event.target.result;
    };
    reader.readAsDataURL(e.target.files[0]);
}

function changeBackground(obj_e) {
    $('#canvasImg').attr('src', obj_e.src);

}