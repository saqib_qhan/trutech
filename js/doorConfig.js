// doorConfig.js

// Written For: TruTech Doors | iScape Apps | Home Revivals LLC
// Written By: Michael McGraw | info@mcg73.com
// July 2013

// Door Configuration | Step 1
// The Below Script is Responsible For:
// Getting the data attributes from the user selected config <img> thumbnail
// Matching to individual door parts
// Building selected parts for display
// Placing parts into user view


var Configuration = function () {
    //when a thumbnail is selected by user match the needed parts for configuration-right display
    $("a.thumbnail > img").click(function () {

        //create variables
        var parts = [];
        var selectedParts = [];
        var doorImgs = [];
        var selectedDoorImgs = [];
        var $dataAttr = $(this).attr('data-doortype');
        var $dframe = $(".doubleFrame");
        var $ddoor = $(".d-door");
        var $drsl = $(".d-rsl");
        var $dlsl = $(".d-lsl");
        var $dtran = $(".d-tran");
        var $sframe = $(".singleFrame");
        var $sdoor = $(".s-door");
        var $srsl = $(".s-rsl");
        var $slsl = $(".s-lsl");
        var $stran = $(".s-tran");
        var $marginFrameSingle = $('.marginFrameSingle');
        var $marginFrameDouble = $('.marginFrameDouble');
        var dataAttrLen = $dataAttr.length;
        var doorStr = $dataAttr.substr(0, 6);
        var lslStr = $dataAttr.substr(7, 4);
        var rslStr = $dataAttr.substr(12, 4);
        var tranStr = $dataAttr.substr(17, 4);
        var singleImg = doorImgs[0];
        var doubleImg = doorImgs[1];
        var leftImg = doorImgs[2];
        var rghtImg = doorImgs[3];
        var tranImg = doorImgs[4];
        var imgURLStr = "url(http://mcg73.com/TTV2/img/doorParts/";

        //variables for step 2 build-a-door
        //mimick configuration show hide for step 2 build-a-door
        var $dframe2 = $(".doubleFrame2");
        var $ddoor2 = $(".d-door2");
        var $drsl2 = $(".d-rsl2");
        var $dlsl2 = $(".d-lsl2");
        var $dtran2 = $(".d-tran2");
        var $sframe2 = $(".singleFrame2");
        var $sdoor2 = $(".s-door2");
        var $srsl2 = $(".s-rsl2");
        var $slsl2 = $(".s-lsl2");
        var $stran2 = $(".s-tran2");


        //hide each on call resets the positon of transom to handle multiple selections
        //w/out these hide()'s the transom will not align properly
        $ddoor.hide();
        $sdoor.hide();
        $srsl.hide();
        $slsl.hide();
        $stran.hide();

        $ddoor2.hide();
        $sdoor2.hide();
        $srsl2.hide();
        $slsl2.hide();
        $stran2.hide();

        $dtran.hide();
        $dtran2.hide();
        $drsl.hide();
        $drsl2.hide();
        $dlsl.hide();
        $dlsl2.hide();
        $dtran.hide();
        $dtran2.hide();

        //reset margins to zero for step 2
        $ddoor2.css('margin', '0px');
        $sdoor2.css('margin', '0px');
        $srsl2.css('margin', '0px');
        $slsl2.css('margin', '0px');
        $stran2.css('margin', '0px');

        $dtran2.css('margin', '0px');
        $drsl2.css('margin', '0px');
        $dlsl2.css('margin', '0px');
        $dtran2.css('margin', '0px');


        //parts array contains the data-attribute sub-string for each part
        parts[0] = doorStr;
        parts[1] = lslStr;
        parts[2] = rslStr;
        parts[3] = tranStr;

        //loop through the parts array and push selected parts into selectedParts array
        for (var i = 0; i < parts.length; i++) {
            if (parts[i] != "null") {
                selectedParts.push(parts[i]);
            }
        }

        //loop through selectedParts[] and attach the string to build the url to selected images
        for (var i = 0; i < selectedParts.length; i++) {
            selectedDoorImgs.push(imgURLStr + selectedParts[i] + ".jpg");
            //console.log(selectedDoorImgs[i]);
            //$( "#right-display" ).append( "<img src=" + selectedDoorImgs[i] + ">" +  "</img>" );
        }

        // initial settings
        if ($('.visible-tablet').is(':visible')) {
            $('#build-a-door-right').find('.visible-tablet').find($marginFrameSingle).find('.single').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
            $('#configuration-right').find('.visible-tablet').find($marginFrameSingle).find('.single').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');

            $('#build-a-door-right').find('.visible-tablet').find($marginFrameDouble).find('.double').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
            $('#configuration-right').find('.visible-tablet').find($marginFrameDouble).find('.double').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
        }
        else if ($('.visible-desktop').is(':visible')) {
            $('#build-a-door-right').find('.visible-desktop').find($marginFrameSingle).find('.single').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
            $('#configuration-right').find('.visible-desktop').find($marginFrameSingle).find('.single').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');

            $('#build-a-door-right').find('.visible-desktop').find($marginFrameDouble).find('.double').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
            $('#configuration-right').find('.visible-desktop').find($marginFrameDouble).find('.double').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
        }
        else if ($('.visible-phone').is(':visible')) {
            $('#build-a-door-right').find('.visible-phone').find($marginFrameSingle).find('.single').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
            $('#configuration-right').find('.visible-phone').find($marginFrameSingle).find('.single').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');

            $('#build-a-door-right').find('.visible-phone').find($marginFrameDouble).find('.double').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
            $('#configuration-right').find('.visible-phone').find($marginFrameDouble).find('.double').removeClass('marginLeft15').removeClass('marginLeft12').removeClass('marginLeft25').removeClass('marginLeft3').removeClass('marginLeftImp3').removeClass('marginLeft33').removeClass('marginLeft20').removeClass('marginLeftImp20').removeClass('marginLeft11').removeClass('marginLeft35').removeClass('marginLeft40').removeClass('marginLeft23').removeClass('marginLeft18').removeClass('marginLeft30').removeClass('marginLeft0').removeClass('marginLeftImp0').removeClass('marginLeft14').removeClass('marginLeftPx45').removeClass('marginLeftPxImp45').removeClass('marginLeftPx50').removeClass('marginLeftPx4').removeClass('marginLeftPx3').removeClass('marginLeftPx49').removeClass('marginLeft7').removeClass('marginLeft22').removeClass('marginTop25');
        }


        $sframe2.css('width', '247px');
        //show the door frame of selected door and show the single door image
        if (selectedParts[0] === "single") {
            $dframe.hide("slow");
            $ddoor.hide("slow");
            $sframe.show("slow").addClass("shownPart");
            $sdoor.show("slow");
            $srsl.hide("slow");
            $slsl.hide("slow");
            $stran.hide("slow");

            //for build-a-door
            $dframe2.hide("slow");
            $ddoor2.hide("slow");
            $sframe2.show("slow");
            $sdoor2.show("slow");
            $srsl2.hide("slow");
            $slsl2.hide("slow");
            $stran2.hide("slow");

        }
        if (selectedParts[0] === "single" && selectedParts[1] == null && selectedParts[2] == null && selectedParts[3] == null) {

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameSingle).addClass('marginLeft30');
            }
            else if ($('.visible-desktop').is(':visible')) {
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft40').addClass('marginTop25');
            }
            else if ($('.visible-phone').is(':visible')) {
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft30').addClass('marginTop25');
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft30').addClass('marginTop25');
            }
        }
        //show double frame and double door img
        if (selectedParts[0] === "double") {
            $sframe.hide("slow");
            $sdoor.hide("slow");
            $dframe.show("slow");
            $ddoor.show("slow");
            $drsl.hide("slow");
            $dlsl.hide("slow");
            $dtran.hide("slow");

            //for build-a-door
            $sframe2.hide("slow");
            $sdoor2.hide("slow");
            $dframe2.show("slow");
//            $dframe2.css("margin-top", "50px");
            $ddoor2.show("slow");
            $drsl2.hide("slow");
            $dlsl2.hide("slow");
            $dtran2.hide("slow");

        }
        if (selectedParts[0] === "double" && selectedParts[1] == null && selectedParts[2] == null && selectedParts[3] == null) {

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameDouble).addClass('marginLeft23');
            }

            else if ($('.visible-desktop').is(':visible')) {
                $('#configuration-right').find($marginFrameDouble).addClass('marginLeft30');
                $('#build-a-door-right').find($marginFrameDouble).addClass('marginLeft30');
            }
            else if ($('.visible-phone').is(':visible')) {
                $('#configuration-right').find($marginFrameDouble).addClass('marginLeft18');
                $('#build-a-door-right').find($marginFrameDouble).addClass('marginLeft18');
            }


        }

        //show single left side light and single door
        if (selectedParts[1] === "left" && selectedParts[0] === "single" && selectedParts[2] == null && selectedParts[3] == null) {
            $slsl.show("slow");
            $sdoor.show("slow");
            $srsl.hide("slow");
            $stran.hide("slow");

            //for build-a-door
            $slsl2.show("slow");
            $sdoor2.show("slow");
            $srsl2.hide("slow");
            $stran2.hide("slow");
            if ($('.visible-phone').is(':visible')) {
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft25');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft25')
            }

            else if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameSingle).addClass('marginLeft23');
            }

            else if ($('.visible-desktop').is(':visible')) {
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft35');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft33');
            }


        }
        //show double left side light and double door
        //if(selectedParts[1] === "left" && selectedParts[0] === "double"){
        //$dlsl.show("slow").clone().appendTo('build-a-door-right');
        //$ddoor.show("slow").clone().appendTo('build-a-door-right');
        //$drsl.hide("slow");
        //$dtran.hide("slow");
        //}
        //show double door and double right side light
        //if(selectedParts[1] === "rght" && selectedParts[0] === "double"){
        //$dlsl.hide("slow");
        //$drsl.show("slow");
        //$drsl.clone().appendTo('build-a-door-right');
        //$ddoor.show("slow");
        //$ddoor.clone().appendTo('build-a-door-right');
        //$dtran.hide("slow");
        //}
        //show single door and single right side light
        if (selectedParts[1] === "rght" && selectedParts[0] === "single" && selectedParts[2] == null && selectedParts[3] == null) {
            $srsl.show("slow");
            $sdoor.show("slow");
            $slsl.hide("slow");
            $stran.hide("slow");

            //for build-a-door
            $srsl2.show("slow");
            $sdoor2.show("slow");
            $slsl2.hide("slow");
            $stran2.hide("slow");

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameSingle).addClass('marginLeft23');
            }

            else if ($('.visible-desktop').is(':visible')) {
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft25');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft35');
            }
            else if ($('.visible-phone').is(':visible')) {
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft25');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft25');
            }

        }
        //show single door and both side lights
        if (selectedParts[0] === "single" && selectedParts[1] === "left" && selectedParts[2] === "rght" && selectedParts[3] == null) {
            $slsl.show("slow");
            $srsl.show("slow");
            $sdoor.show("slow");
            $stran.hide("slow");

            //for build-a-door
            $slsl2.show("slow");
            $srsl2.show("slow");
            $sdoor2.show("slow");
            $stran2.hide("slow");

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameSingle).addClass('marginLeft18');
            }

            else if ($('.visible-desktop').is(':visible')) {
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft30');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft30');
            }
            else if ($('.visible-phone').is(':visible')) {
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft18');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft18');
            }


        }
        //show double door and both side lights
        if (selectedParts[0] === "double" && selectedParts[1] === "left" && selectedParts[2] === "rght" && selectedParts[3] == null) {
            $dlsl.show("slow");
            $drsl.show("slow");
            $ddoor.show("slow");
            $stran.hide("slow");

            //for build-a-door
            $dlsl2.show("slow");
            $drsl2.show("slow");
            $ddoor2.show("slow");
            $stran2.hide("slow");

            if ($('.visible-phone').is(':visible')) {
                $('#configuration-right').find($marginFrameDouble).addClass('marginLeft7');
                $('#build-a-door-right').find($marginFrameDouble).addClass('marginLeft7');
            }

        }
        // show double door and left sidelite
        if (selectedParts[0] === "double" && selectedParts[1] === "left" && selectedParts[2] == null && selectedParts[3] == null) {
            $dlsl.show("slow");
            $ddoor.show("slow");

            //for build-a-door
            $dlsl2.show("slow");
            $ddoor2.show("slow");

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameDouble).addClass('marginLeft15');
            }


        }
        // show double door and right sidelite
        if (selectedParts[0] === "double" && selectedParts[1] === "rght" && selectedParts[2] == null && selectedParts[3] == null) {
            $drsl.show("slow");
            $ddoor.show("slow");

            //for build-a-door
            $drsl2.show("slow");
            $ddoor2.show("slow");

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameDouble).addClass('marginLeft15');
            }

        }

        //show double door transom and left side light
        if (selectedParts[0] === "double" && selectedParts[2] === "tran" && selectedParts[1] === "left" && selectedParts[3] == null) {
            $dtran.css("width", "285px");
//            $dtran.addClass('marginLeftPx4');

            if ($('.visible-desktop').is(':visible')) {
                $dtran.css("width", "285px");
                $dtran.addClass('marginLeft0');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran.css("width", "248px");
                $dtran.addClass('marginLeft0');
            }
            else if ($('.visible-phone').is(':visible')) {
                $dtran.css("width", "181px");
            }

            $dlsl.show("slow");
            $ddoor.show("slow");
            $dtran.show("slow");

            //for build a door
            $dtran2.css("width", "292px");
            if ($('.visible-desktop').is(':visible')) {
                $dtran2.css("width", "289px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran2.css("width", "248px");
            }
            else if ($('.visible-phone').is(':visible')) {
                $dtran2.css("width", "180px");
            }
//            $dtran2.css("margin-left", "0px");
            $dlsl2.addClass('marginLeftPx3');
            $dlsl2.show("slow");
            $ddoor2.show("slow");
            $dtran2.show("slow");
        }
        //show double door transom and right side light
        if (selectedParts[0] === "double" && selectedParts[2] === "tran" && selectedParts[1] === "rght" && selectedParts[3] == null) {
            $dtran.css("width", "285px");
//            $dtran.addClass('marginLeft49');

            if ($('.visible-desktop').is(':visible')) {
                $dtran.css("width", "285px");
                $dtran.addClass('marginLeft0');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran.css("width", "250px");
                $dtran.addClass('marginLeft0');

            }
            else if ($('.visible-phone').is(':visible')) {
                $dtran.css("width", "181px");

            }

            $ddoor.show("slow");
            $drsl.show("slow");
            $dtran.show("slow");

            //for build a door
            $dtran2.css("width", "290px");
            if ($('.visible-desktop').is(':visible')) {
                $dtran2.css("width", "290px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran2.css("width", "250px");
            }
            else if ($('.visible-phone').is(':visible')) {
                $dtran2.css("width", "181px");
            }
//            $dtran2.css("margin-left", "49px");
            $dtran2.addClass('margin0');
            $ddoor2.show("slow");
            $drsl2.show("slow");
            $dtran2.show("slow");

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameDouble).addClass('marginLeft14');
            }

        }

        //show single door transom and both side lights
        if (selectedParts[0] === "single" && selectedParts[3] === "tran" && selectedParts[1] === "left" && selectedParts[2] === "rght") {
            $stran.css("width", "244px");
            $stran.addClass('marginLeft0');
            if ($('.visible-desktop').is(':visible')) {
                $stran.css("width", "238px");
                $stran.addClass('marginLeft0');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $stran.css("width", "225px");
            }
            else if ($('.visible-phone').is(':visible')) {
                $stran.css("width", "161px");
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft18');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft18')
            }
            $slsl.show("slow");
            $srsl.show("slow");
            $stran.show("slow");

            //for build-a-door
            $stran2.css("width", "244px");
            if ($('.visible-desktop').is(':visible')) {
                $stran2.css("width", "239px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $stran2.css("width", "239px");
            }
            else if ($('.visible-phone').is(':visible')) {
                $stran2.css("width", "160px");
            }

//			$stran2.css("margin-left", "0px");
            $stran2.css("margin", "0px");
            $slsl2.show("slow");
            $srsl2.show("slow");
            $stran2.show("slow");


        }
        //show single door transom
        if (selectedParts[0] === "single" && selectedParts[1] === "tran" && selectedParts[2] == null && selectedParts[3] == null) {
            $stran.css("width", "147px");
//            $stran.addClass('marginLeftPx49');

            if ($('.visible-desktop').is(':visible')) {
                $stran.css("width", "147px");
                $stran.addClass('marginLeftPx45');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $stran.css("width", "147px");
                $stran.addClass('marginLeft0');
            }
            else if ($('.visible-phone').is(':visible')) {
                $stran.css("width", "100px");
            }

            $stran.show("slow");
            $sdoor.show("slow");

            //for build-a-door
            $stran2.css("width", "150px");
            if ($('.visible-desktop').is(':visible')) {
                $stran2.css("width", "147px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $stran2.css("width", "147px");
            }

            else if ($('.visible-phone').is(':visible')) {
                $stran2.css("width", "100px");
            }

//			$stran2.css("margin-left", "45px");
            $stran2.css("margin", "0px");
            $stran2.show("slow");

        }
        //show double door transom and both side lights
        if (selectedParts[0] === "double" && selectedParts[3] === "tran" && selectedParts[1] === "left" && selectedParts[2] === "rght") {
            $dtran.css("width", "340px");
//            $dtran.addClass('marginLeftPx4');

            if ($('.visible-desktop').is(':visible')) {
                $dtran.css("width", "340px");
                $dtran.addClass('marginLeft0');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran.css("width", "300px");
                $dtran.addClass('marginLeft0');

            }
            else if ($('.visible-phone').is(':visible')) {
                $('#configuration-right').find($marginFrameDouble).addClass('marginLeft7');
                $('#build-a-door-right').find($marginFrameDouble).addClass('marginLeft7');
            }


            //$dtran.css("margin-left", "-2px");
            //$dtran.css("margin-top", "-15px");
            $dlsl.show("slow");
            $ddoor.show("slow");
            $drsl.show("slow");
            $dtran.show("slow");

            //for build a door
            $dtran2.css("width", "340px");
            if ($('.visible-desktop').is(':visible')) {
                $dtran2.css("width", "340px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran2.css("width", "300px");
            }
//			$dtran2.css("margin-left", "-4px");
            $dtran2.addClass('margin0');
            //$dtran2.css("margin-left", "-2px");
            //$dtran2.css("margin-top", "-15px");
            $dlsl2.show("slow");
            $ddoor2.show("slow");
            $drsl2.show("slow");
            $dtran2.show("slow");

            if ($('.visible-tablet').is(':visible')) {
                $('.visible-tablet').find($marginFrameDouble).addClass('marginLeft7');
            }

            else if ($('.visible-desktop').is(':visible')) {
                $('#configuration-right').find($marginFrameDouble).addClass('marginLeft22');
                $('#build-a-door-right').find($marginFrameDouble).addClass('marginLeft22');
            }

        }
        //show double door transom
        if (selectedParts[0] === "double" && selectedParts[1] === "tran" && selectedParts[2] == null && selectedParts[3] == null) {
            $dtran.css("width", "240px");
//            $dtran.addClass('marginLeftPx50');

            if ($('.visible-desktop').is(':visible')) {
                $dtran.css("width", "240px");
                $dtran.addClass('marginLeft0');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran.css("width", "200px");
                $dtran.addClass('marginLeft0');
            }
            else if ($('.visible-phone').is(':visible')) {
                $dtran.css("width", "152px");
                $('#configuration-right').find($marginFrameDouble).addClass('marginLeft20');
                $('#build-a-door-right').find($marginFrameDouble).addClass('marginLeft20');
            }

            $ddoor.show("slow");
            $dtran.show("slow");

            //for build a door
            $dtran2.css("width", "240px");
            if ($('.visible-desktop').is(':visible')) {
                $dtran2.css("width", "240px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $dtran2.css("width", "200px");
            }
            else if ($('.visible-phone').is(':visible')) {
                $dtran2.css("width", "150px");
            }
//			$dtran2.css("margin-left", "50px");
            $dtran2.css("margin", "0px");
            $ddoor2.show("slow");
            $dtran2.show("slow");
        }
        //show single door transom and left side light
        if (selectedParts[2] === "tran" && selectedParts[1] === "left" && selectedParts[0] === "single" && selectedParts[3] == null) {
            $stran.css("width", "196px");
            if ($('.visible-desktop').is(':visible')) {
                $stran.css("width", "192px");
                $stran.addClass('marginLeft0');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $stran.css("width", "185px");
            }
            else if ($('.visible-phone').is(':visible')) {
                $stran.css("width", "130px");
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft25');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft25')
            }

            $stran.addClass('marginLeft0');
            $sdoor.show('slow');
            $stran.show("slow");
            $slsl.show("slow");
            $srsl.hide("slow");

            //for build a door
            $sframe2.css('width', '240px');
            $stran2.css("width", "196px");

            if ($('.visible-desktop').is(':visible')) {
                $sframe2.css('width', '218px');
                $stran2.css("width", "193px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $sframe2.css('width', '218px');
                $stran2.css("width", "193px");
            }

            else if ($('.visible-phone').is(':visible')) {
                $stran2.css("width", "130px");
            }

//			$stran2.css("margin-left", "0px");
            $stran2.css("margin", "0px");
            $stran2.show("slow");
            $slsl2.show("slow");
            $srsl2.hide("slow");
        }
        //show single door transom and right side light
        else if (selectedParts[2] === "tran" && selectedParts[1] === "rght" && selectedParts[0] === "single" && selectedParts[3] == null) {
            $stran.css("width", "196px");
//            $stran.addClass('marginLeftPx49');

            if ($('.visible-desktop').is(':visible')) {
                $stran.css("width", "193px");
                $stran.addClass('marginLeftPx45');
            }
            else if ($('.visible-tablet').is(':visible')) {
                $stran.css("width", "185px");
                $stran.addClass('marginLeft0');
            }
            else if ($('.visible-phone').is(':visible')) {
                $stran.css("width", "131px");
                $stran.addClass('marginLeft0');
                $('#configuration-right').find($marginFrameSingle).addClass('marginLeft23');
                $('#build-a-door-right').find($marginFrameSingle).addClass('marginLeft23')
            }

            $stran.show("slow");
            $srsl.show("slow");
            $slsl.hide("slow");

            //for build a door
            $stran2.css("width", "196px");
            if ($('.visible-desktop').is(':visible')) {
                $stran2.css("width", "193px");
            }
            else if ($('.visible-tablet').is(':visible')) {
                $stran2.css("width", "193px");
            }
            else if ($('.visible-phone').is(':visible')) {
                $stran2.css("width", "130px");
            }

//			$stran2.css("margin-left", "46px");
            $stran2.addClass('margin0');
            $stran2.show("slow");
            $srsl2.show("slow");
            $slsl2.hide("slow");
        }
        //show double door transom and left side light
        /*if(selectedParts[2] === "tran" && selectedParts[1] === "left" && selectedParts[0] === "double"){
         $dtran.css("width", "289px");
         $dtran.css("margin-left", "0px");
         $ddoor.show("slow");
         $dtran.show("slow");
         $dlsl.show("slow");
         $drsl.hide("slow");
         }

         //show double door transom and right side light
         if(selectedParts[2] === "tran" && selectedParts[1] === "rght" && selectedParts[0] === "double"){
         $stran.css("width", "289px");
         $dtran.css("margin-left", "49px");
         $ddoor.show("slow");
         $dtran.show("slow");
         $drsl.show("slow");
         $dlsl.hide("slow");
         }*/

        //$("#singleFrame").clone().appendTo("#build-a-door-right");
        //alert(selectedParts.toString());

        return true;

    }); //end of click event function

} //end of Configuration


