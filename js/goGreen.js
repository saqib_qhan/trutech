// JavaScript Document

var goGreen = function () {
    $("a.thumbnail > img").click(function () {
        $('#redBack').removeClass("label-important").addClass("label-success");
        $('#config-next button').css("color", "rgba(0,0,102,0.8)");
        $("#nextBtn").removeClass("btn-inverse").addClass("btn-success");
        $("#doorBtn").addClass('active');
    });
    return true;
}//goGreen()

var handleActiveClass = function () {
    $("#nav-list li").css("background", "#666");
    $("#nav-list li  a").css("color", "ivory");
    $("ul > #nav-list li").mouseover(function () {
        if ($(this).css("background", "#666")) {
            $(this).css("background", "#0C2757");
        }
    });
}

//this runs on doc ready
var hideElements = function () {
    var $configuration = $("#configuration");
    var $buildADoor = $("#build-a-door");
    var $tryItOn = $("#try-it-on");
    $buildADoor.hide();
}

$(".thumbnail").click(function () {
    $(".thumbnail").css("border", "white");
    $(this).css("border", "1px solid #0088CC");
});

$("#doorTypeRightBtns").click(function () {
    $("#glassCollectionLeft").show("fast");
    $("#glassCollectionRight").show("fast");
});
//show hide for glass collection step 2 part 1
$("#solidDoorBtn").click(function () {
    $("#glassStyle").hide();
    $("#glassCollection").hide();
});
$("#glassDoorBtn").click(function () {
    $("#gStyleGlass > button").removeClass('active');
    $("#glassCollection").show("fast");
    $("#glassStyle").show("fast");
});
$("#gStyleGlass > button").click(function () {
    $("#glassCollectionLeft").show("fast");
    $("#glassCollectionRight").show("fast");
});
//show hide for step 1 [door]
function doorBtnClicked(obje) {
    $("#doorBtn").addClass('active');
    $("#slBtn").removeClass('active');
    $("#tranBtn").removeClass('active');
    $("#sideLightOptions").hide("fast");
    $("#trannyOptions").hide("fast");
    $("#doorOptions").show("fast");
}
//show hide for step 1 [sidelights]
function slBtnClicked(obje) {
    if ($(obje).hasClass('disabled')) {
        var answer = confirm("Would like to select a configuration with a sidelight?"); // the whole script stops until the user has made a decision!

        // process answer
        if (answer) {
            $('#door-btn-group > #buildDoorPrevBtn').click();
        }
        else {
            return false;
        }
    }
    else {
        $("#doorBtn").removeClass('active');
        $("#slBtn").addClass('active');
        $("#tranBtn").removeClass('active');
        $("#sideLightOptions").show("fast");
        $("#trannyOptions").hide("fast");
        $("#doorOptions").hide("fast");
    }
}
//show hide for step 1 [transom]
function tranBtnClicked(obje) {
    if ($(obje).hasClass('disabled')) {
        var answer = confirm("Would like to select a configuration with a transom?"); // the whole script stops until the user has made a decision!

        // process answer
        if (answer) {
            $('#door-btn-group > #buildDoorPrevBtn').click();
        }
        else {
            return false;
        }
    }
    else {

        $("#doorBtn").removeClass('active');
        $("#slBtn").removeClass('active');
        $("#tranBtn").addClass('active');
        $("#sideLightOptions").hide("fast");
        $("#trannyOptions").show("fast");
        $("#doorOptions").hide("fast");
    }
}

$("#glassOpBtns > li").click(function () {
    $(this).addClass('active');
    if ($("#glassOpBtns > li").closest('li').hasClass('active')) {
        $("#glassOpBtns > li").closest('li').removeClass('active');
    }
});

$(".ttGlass").click(function () {
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
});
//next button on step 1 
$("#nextBtn").click(function () {
    if (goGreen()) {
        $("#configuration").hide();
        $("#build-a-door").show('fast');
        $("#doorBtn").addClass('active');
        $("#sideLightOptions").hide("fast");
        $("#trannyOptions").hide("fast");
        $("#doorOptions").show("fast");
        $("#step1").removeClass("label-success").addClass("label-inverse");
        $("#step2").removeClass("label-info").addClass("label-success");

        var type_id = localStorage.getItem("type-id");
        if (type_id.search('left') == -1 && type_id.search('right') == -1) {
            $('#collapseFive').parent().hide();
//            $('#SideLiteCont').hide();
        }
        else {
            $('#collapseFive').parent().show();
//            $('#SideLiteCont').show();
        }
        if (type_id.search('tran') == -1) {
            $('#collapseSix').parent().hide();
//            $('#myCarousel3').hide();
        }
        else {
            $('#collapseSix').parent().show();
//            $('#myCarousel3').show();
        }
        $("#config-left-top").niceScroll({styler: "fb", cursorcolor: "#000"}).hide();
        $("#config-left-mid").niceScroll({styler: "fb", cursorcolor: "#000"}).hide();
    }

    if (goGreen === false) {
        alert("select configuration to continue");
    }
});

//start over and  previous buttons on try-it-on [step 3]
$("#try-prev").click(function () {
    $("#try-it-on").hide("fast");
    $("#configuration").hide();
    $("#build-a-door").show("fast");
    $("#step2").removeClass("label-inverse").addClass("label-success");
    $("#step3").removeClass("label-success").addClass("label-info");

//	$("#uploaderCloned").empty();
});
$("#try-start").click(function () {
    var answer = confirm("All the selections will be deleted and you will be directed to the first step, do you want to continue?"); // the whole script stops until the user has made a decision!
// process answer
    if (answer) {
        window.location.reload();
//        $("#try-it-on").hide("fast");
//        $("#build-a-door").hide();
//        $("#configuration").show("fast");
//        $("#step1").removeClass("label-inverse").addClass("label-success");
//        $("#step2").removeClass("label-inverse").addClass("label-info");
//        $("#step3").removeClass("label-success").addClass("label-info");
    }
    else {
        return false;
    }
});

//previous and next buttons on build-a-door [step 2]
$("#buildDoorNextBtn").click(function () {
//    $("#build-a-door").hide("fast");
//    $("#configuration").hide();
//    $("#try-it-on").show("fast");
//    $("#step2").removeClass("label-success").addClass("label-inverse");
//    $("#step3").removeClass("label-info").addClass("label-success");
//
//    var kids = $('.shownPart').clone();
//    kids.appendTo("#clonedParts");

});


$("#buildDoorPrevBtn").click(function () {
    $("#build-a-door").hide();
    $("#try-it-on").hide("fast");
    $("#configuration").show("fast");
    $("#doorBtn").removeClass('active');
    $("#slBtn").removeClass('active');
    $("#tranBtn").removeClass('active');
    $("#step2").removeClass("label-success").addClass("label-info");
    $("#step1").removeClass("label-inverse").addClass("label-success");
    $("#config-left-top").niceScroll({styler: "fb", cursorcolor: "#000"}).show();
    $("#config-left-mid").niceScroll({styler: "fb", cursorcolor: "#000"}).show();
});


// Image replacement [Author: Saqib]
function right_side_image(image, obj_e) {
    var active_tab = $('#buildDoorBtns').find('.active').attr('id');
    typeOfImage = $(obj_e).attr('id');
    if (typeOfImage == "typeImage0") {
        if ($('.build-door-right-display > .doubleFrame2').is(':visible')) {
            if ($('.doubleFrame2 > .d-door2').is(':visible')) {
                $('.doubleFrame2 > .d-door2').find('.asset-img').hide();
                $('.doubleFrame2 > .d-door2').find('.db-img').hide();
                $(image).insertAfter('.doubleFrame2 > .d-door2 > .asset-img');
            }
        }
        else if ($('.build-door-right-display > .singleFrame2').is(':visible')) {
            if ($('.singleFrame2 > .s-door2').is(':visible')) {
                $('.singleFrame2 > .s-door2').find('.asset-img').hide();
                $('.singleFrame2 > .s-door2').find('.db-img').hide();
                $(image).insertAfter('.singleFrame2 > .s-door2 > .asset-img');
            }
        }
        if ($('#collapseFiveDiv').is(":visible")) {
            $('#collapseFour').slideUp('slow');
            $('#collapseFive').css('height', 'auto');
            $('#collapseFive').slideDown('slow');
//            $('#collapseFive').click();
        }
        else {
            $('#collapseFour').slideUp('slow');
            $('#collapseSix').css('height', 'auto');
            $('#collapseSix').slideDown('slow');

        }

    }
    else if (typeOfImage == "typeImage1") {
        if ($('.build-door-right-display > .doubleFrame2').is(':visible')) {
            if ($('.doubleFrame2 > .d-lsl2').is(':visible')) {
                $('.doubleFrame2 > .d-lsl2').find('.asset-img').hide();
                $('.doubleFrame2 > .d-lsl2').find('.db-img').hide();
                $(image).insertAfter('.doubleFrame2 > .d-lsl2 > .asset-img');
            }
            if ($('.doubleFrame2 > .d-rsl2').is(':visible')) {
                $('.doubleFrame2 > .d-rsl2').find('.asset-img').hide();
                $('.doubleFrame2 > .d-rsl2').find('.db-img').hide();
                $(image).insertAfter('.doubleFrame2 > .d-rsl2 > .asset-img');
                $('.doubleFrame2 > .d-rsl2 > img.db-img').addClass('rotateImg');
            }
        }
        else if ($('.build-door-right-display > .singleFrame2').is(':visible')) {
            if ($('.singleFrame2 > .s-lsl2').is(':visible')) {
                $('.singleFrame2 > .s-lsl2').find('.asset-img').hide();
                $('.singleFrame2 > .s-lsl2').find('.db-img').hide();
                $(image).insertAfter('.singleFrame2 > .s-lsl2 > .asset-img');
            }
            if ($('.singleFrame2 > .s-rsl2').is(':visible')) {
                $('.singleFrame2 > .s-rsl2').find('.asset-img').hide();
                $('.singleFrame2 > .s-rsl2').find('.db-img').hide();
                $(image).insertAfter('.singleFrame2 > .s-rsl2 > .asset-img');
                $('.singleFrame2 > .s-rsl2 > img.db-img').addClass('rotateImg');
            }
        }
        if ($('#collapseSixDiv').is(":visible")) {
//            $('#collapseSix').click();
//            customeCollapsed('collapseSix');
            $('#collapseFive').slideUp('slow');
            $('#collapseSix').css('height', 'auto');
            $('#collapseSix').slideDown('slow');
        }

    }
    else if (typeOfImage == "typeImage2") {
        if ($('.build-door-right-display > .doubleFrame2').is(':visible')) {
            if ($('.doubleFrame2 > .d-tran2').is(':visible')) {
                $('.doubleFrame2 > .d-tran2').find('.asset-img').hide();
                $('.doubleFrame2 > .d-tran2').find('.db-img').hide();
                $(image).insertAfter('.doubleFrame2 > .d-tran2 > .asset-img');
            }
        }
        else if ($('.build-door-right-display > .singleFrame2').is(':visible')) {
            if ($('.singleFrame2 > .s-tran2').is(':visible')) {
                $('.singleFrame2 > .s-tran2').find('.asset-img').hide();
                $('.singleFrame2 > .s-tran2').find('.db-img').hide();
                $(image).insertAfter('.singleFrame2 > .s-tran2 > .asset-img');
            }
        }
    }

// check if to activate the next button or not
    div_count = 0;
    img_count = 0;
    if ($('.build-door-right-display > .doubleFrame2').is(':visible')) {
        $('.build-door-right-display > .doubleFrame2 > .buildDoor').each(function () {
            if ($(this).is(':visible')) {
                div_count = div_count + 1;
            }
            if ($(this).find('img').is(':visible')) {
                if ($(this).find('img.db-img').is(':visible')) {
                    img_count = img_count + 1;
                }
            }
        });
    }
    else if ($('.build-door-right-display > .singleFrame2').is(':visible')) {
        $('.build-door-right-display > .singleFrame2 > .buildDoor').each(function () {
            if ($(this).is(':visible')) {
                div_count = div_count + 1;
            }
            if ($(this).find('img').is(':visible')) {
                if ($(this).find('img.db-img').is(':visible')) {
                    img_count = img_count + 1;
                }
            }
        });
    }
    if (div_count != 0 && img_count != 0 && div_count == img_count) {
        $('#door-btn-group > #buildDoorNextBtn').removeClass('disabled');
    }
    else {
        $('#door-btn-group > #buildDoorNextBtn').addClass('disabled');
    }
}

function customeCollapsed(type) {
    hidden = true;
    if ($('#' + type).hasClass('in')) {
        hidden = false;
    }
    $('#accordion').find('.in').removeClass('in');
    if (hidden) {
        $('#' + type).addClass('in');
    }

}
